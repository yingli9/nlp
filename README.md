# NLP

Data Science intrigued me profoundly because the powerful approaches enable me to explore and predict both natural phenomena and human behaviors from an integral perspective, therefore providing big picture visions. In recent years, I leverage emerging Machine Learning and Big Data Analytics technology to diagnose business operation performance, identify opportunities for improvement and optimization, and drive engaging experiences for worldwide users.

Here I'm compiling a repository of very interesting data mining projects.  

I would like to hear what you want to know! Grateful if you could take a moment to provide your question(s) or comments. 

Thank you. Have a wonderful day!~ 
